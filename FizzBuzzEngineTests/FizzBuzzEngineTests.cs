﻿using NUnit.Framework;
using System;
using FizzBuzzGame;
using System.Collections.Generic;

namespace FizzBuzzGameTests
{
	
	[TestFixture]
	public class FizzBuzzEngineTests
	{
		private FizzBuzzEngine engine;

		[SetUp]
		public void setUp ()
		{
			List<IRule> _rules = new List<IRule> ();
			_rules.Add (new FizzRule ());
			_rules.Add (new BuzzRule ());
			_rules.Add (new BangRule ());
			_rules.Add (new TheAnswerRule ());

			engine = new FizzBuzzEngine (_rules);
		}

		[Test]
		public void CanSayOne ()
		{
			// Act
			var one = engine.Say (1);

			// Assert
			Assert.AreEqual ("1", one);
		}

		[Test]
		public void CanSayFizz ()
		{
			// Act
			var fizz = engine.Say (3);

			// Assert
			Assert.AreEqual ("Fizz", fizz);
		}

		[Test]
		public void CanStillSayFizz ()
		{
			// Act
			var fizz = engine.Say (6);

			// Assert
			Assert.AreEqual ("Fizz", fizz);
		}

		[Test]
		public void CanSayBuzz ()
		{
			// Act
			var buzz = engine.Say (5);

			// Assert
			Assert.AreEqual ("Buzz", buzz);
		}

		[Test]
		public void CanSayFizzBuzz ()
		{
			// Act
			var fizzbuzz = engine.Say (15);

			// Assert
			Assert.AreEqual ("FizzBuzz", fizzbuzz);
		}

		[ExpectedException (typeof(NoDefinedeRulesException))]
		[Test]
		public void WillExplodeWhenNoRules ()
		{
			// Arrange
			engine = new FizzBuzzEngine (null);

			// Act
			var fizzbuzz = engine.Say (15);

			// Assert
			Assert.AreEqual ("FizzBuzz", fizzbuzz);
		}

		[Test]
		public void CanSayBang ()
		{
			// Act
			var bang = engine.Say (7);

			// Assert
			Assert.AreEqual ("Bang", bang);
		}

		[Test]
		public void CanSayFizzBang ()
		{
			// Act
			var fizzBang = engine.Say (21);

			// Assert
			Assert.AreEqual ("FizzBang", fizzBang);
		}

		[Test]
		public void CanSayFizzBuzzBang ()
		{
			// Act
			var fizzBuzzBang = engine.Say (105);

			// Assert
			Assert.AreEqual ("FizzBuzzBang", fizzBuzzBang);
		}

		[Test]
		public void RuleInjectionOrderDoesntMatter ()
		{
			// Arrange
			List<IRule> _rules = new List<IRule> ();
			_rules.Add (new BangRule ());
			_rules.Add (new FizzRule ());
			_rules.Add (new BuzzRule ());

			engine = new FizzBuzzEngine (_rules);

			// Act
			var fizzBang = engine.Say (21);

			// Assert
			Assert.AreEqual ("FizzBang", fizzBang);
		}

		[Test]
		public void CantAddTheSameRuleTwice ()
		{
			// Arrange
			List<IRule> _rules = new List<IRule> ();
			_rules.Add (new BangRule ());
			_rules.Add (new FizzRule ());
			_rules.Add (new BuzzRule ());
			_rules.Add (new BangRule ());

			engine = new FizzBuzzEngine (_rules);

			// Act
			var fizzBang = engine.Say (21);

			// Assert
			Assert.AreEqual ("FizzBang", fizzBang);
		}

		[Test]
		public void CanSayTheAnswerTo42()
		{
			// Act
			var theAnswer = engine.Say (42);

			// Assert
			Assert.AreEqual ("The answer to the meaning of life, the universe, and everything", theAnswer);
		}
	}
}

